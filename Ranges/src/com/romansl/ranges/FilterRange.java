package com.romansl.ranges;

public class FilterRange<T> extends Range<T> {
    private final Range<T> range;
    private final Predicate<? super T> predicate;

    public FilterRange(final Range<T> range, final Predicate<? super T> predicate) {
        this.range = range;
        this.predicate = predicate;
        advice();
    }

    private void advice() {
        final Range<T> r = range;
        final Predicate<? super T> p = predicate;

        for(; !r.empty(); r.popFront()) {
            if (p.apply(r.front())) {
                break;
            }
        }
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public void popFront() {
        range.popFront();
        advice();
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }
}
