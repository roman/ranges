package com.romansl.ranges;

public class BidirCharacterRange extends BidirRange<Character> {
    private final CharSequence chars;
    private int front;
    private int back;

    public BidirCharacterRange(final CharSequence chars) {
        this.chars = chars;
        front = 0;
        back = chars.length() - 1;
    }

    public BidirCharacterRange(final CharSequence chars, final int offset, final int length) {
        this.chars = chars;
        front = offset;
        back = offset + length - 1;
    }

    @Override
    public Character front() {
        return chars.charAt(front);
    }

    @Override
    public Character back() {
        return chars.charAt(back);
    }

    @Override
    public void popFront() {
        ++front;
    }

    @Override
    public void popBack() {
        --back;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final Character v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBack(final Character v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }
}
