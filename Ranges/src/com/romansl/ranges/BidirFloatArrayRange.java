package com.romansl.ranges;

import java.util.Iterator;

public class BidirFloatArrayRange extends BidirRange<Float> {
    private final float[] array;
    private int front;
    private int back;

    public BidirFloatArrayRange(final float[] array) {
        this.array = array;
        front = 0;
        back = array.length - 1;
    }

    public BidirFloatArrayRange(final float[] array, final int offset, final int length) {
        this.array = array;
        front = offset;
        back = offset + length - 1;
    }

    @Override
    public Float front() {
        return array[front];
    }

    @Override
    public Float back() {
        return array[back];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final Float v) {
        array[front] = v;
    }

    @Override
    public void setBack(final Float v) {
        array[back] = v;
    }
}
