package com.romansl.ranges;

public class ChainRange2<T> extends Range<T> {
    final Range<T> r2;
    Range<T> current;

    public ChainRange2(final Range<T> r1, final Range<T> r2) {
        this.r2 = r2;
        current = r1;
    }

    @Override
    public T front() {
        return current.front();
    }

    @Override
    public void popFront() {
        current.popFront();
    }

    @Override
    public boolean empty() {
        if (current.empty()) {
            if (current == r2)
                return true;
            else {
                current = r2;
                return current.empty();
            }
        }

        return false;
    }

    @Override
    public void setFront(final T v) {
        current.setFront(v);
    }
}
