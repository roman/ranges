package com.romansl.ranges;

public class BidirFilterRange<T> extends BidirRange<T> {
    private final BidirRange<T> range;
    private final Predicate<? super T> predicate;

    public BidirFilterRange(final BidirRange<T> range, final Predicate<? super T> predicate) {
        this.range = range;
        this.predicate = predicate;

        adviceFront();
        adviceBack();
    }

    private void adviceBack() {
        final BidirRange<T> r = range;
        final Predicate<? super T> p = predicate;

        for(; !r.empty(); r.popBack()) {
            if (p.apply(r.back())) {
                break;
            }
        }
    }

    private void adviceFront() {
        final Range<T> r = range;
        final Predicate<? super T> p = predicate;

        for(; !r.empty(); r.popFront()) {
            if (p.apply(r.front())) {
                break;
            }
        }
    }

    @Override
    public T back() {
        return range.back();
    }

    @Override
    public void popBack() {
        range.popBack();
        adviceBack();
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public void popFront() {
        range.popFront();
        adviceFront();
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }

    @Override
    public void setBack(final T v) {
        range.setBack(v);
    }
}
