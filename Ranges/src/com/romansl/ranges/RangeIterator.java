package com.romansl.ranges;

import java.util.Iterator;

public class RangeIterator<T> implements Iterator<T>{
    private final Range<T> range;

    public RangeIterator(final Range<T> range) {
        this.range = range;
    }

    @Override
    public boolean hasNext() {
        return !range.empty();
    }

    @Override
    public T next() {
        final T front = range.front();
        range.popFront();
        return front;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
