package com.romansl.ranges;

public class GenericChainRange<T> extends Range<T> {
    private final Range<T>[] ranges;
    private int current;

    GenericChainRange(final Range<T>[] ranges) {
        this.ranges = ranges;
    }

    @Override
    public T front() {
        return ranges[current].front();
    }

    @Override
    public void popFront() {
        ranges[current].popFront();
    }

    @Override
    public boolean empty() {
        while (current < ranges.length) {
            if (ranges[current].empty()) {
                current++;
            } else
                return false;
        }

        return true;
    }

    @Override
    public void setFront(final T v) {
        ranges[current].setFront(v);
    }
}
