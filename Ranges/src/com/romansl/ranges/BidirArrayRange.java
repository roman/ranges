package com.romansl.ranges;

import java.util.Iterator;

public class BidirArrayRange<T> extends BidirRange<T> implements Iterator<T> {
    private final T[] array;
    private int front;
    private int back;

    public BidirArrayRange(final T[] array) {
        this.array = array;
        front = 0;
        back = array.length - 1;
    }

    public BidirArrayRange(final T[] array, final int offset, final int length) {
        this.array = array;
        front = offset;
        back = offset + length - 1;
    }

    @Override
    public T front() {
        return array[front];
    }

    @Override
    public T back() {
        return array[back];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final T v) {
        array[front] = v;
    }

    @Override
    public void setBack(final T v) {
        array[back] = v;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return front < array.length;
    }

    @Override
    public T next() {
        return array[front++];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
