package com.romansl.ranges;

public class Functions {
    public static final Function<Object, String> TO_STRING = new Function<Object, String>() {
        @Override
        public String apply(final Object p) {
            return p.toString();
        }
    };
    public static final Function<String, Integer> TO_INT = new Function<String, Integer>() {
        @Override
        public Integer apply(final String p) {
            return Integer.valueOf(p);
        }
    };
    public static final Function<String, Long> TO_LONG = new Function<String, Long>() {
        @Override
        public Long apply(final String p) {
            return Long.valueOf(p);
        }
    };
    public static final Function<String, Float> TO_FLOAT = new Function<String, Float>() {
        @Override
        public Float apply(final String p) {
            return Float.valueOf(p);
        }
    };
    public static final Function<String, Double> TO_DOUBLE = new Function<String, Double>() {
        @Override
        public Double apply(final String p) {
            return Double.valueOf(p);
        }
    };
}
