package com.romansl.ranges;

public class RepeatValueRange<T> extends Range<T> {
    private final T v;
    private int i;

    public RepeatValueRange(final T v, final int n) {
        this.v = v;
        i = n;
    }

    @Override
    public T front() {
        return v;
    }

    @Override
    public void popFront() {
        i--;
    }

    @Override
    public boolean empty() {
        return i <= 0;
    }

    @Override
    public void setFront(final T v) {
        // no action
    }
}
