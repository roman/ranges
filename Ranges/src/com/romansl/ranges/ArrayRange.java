package com.romansl.ranges;

import java.util.Iterator;

public class ArrayRange<T> extends Range<T> implements Iterator<T> {
    private final T[] array;
    private int front;

    public ArrayRange(final T[] array) {
        this.array = array;
        this.front = 0;
    }

    public ArrayRange(final T[] array, final int front) {
        this.array = array;
        this.front = front;
    }

    @Override
    public T front() {
        return array[front];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public boolean empty() {
        return front >= array.length;
    }

    @Override
    public void setFront(final T v) {
        array[front] = v;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return front < array.length;
    }

    @Override
    public T next() {
        return array[front++];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
