package com.romansl.ranges;

import java.util.regex.Matcher;

public abstract class RegexMatchRange<T extends CharSequence> extends Range<T> {
    protected final T input;
    private final Matcher matcher;
    private final int length;
    protected int start;
    protected int end;

    public RegexMatchRange(final T input, final Matcher matcher) {
        this.input = input;
        this.matcher = matcher;

        length = input.length();

        if (length == 0) {
            start = -1;
        } else {
            advice();
        }
    }

    @Override
    public void popFront() {
        if (end < length) {
            start = matcher.end();
            advice();
        } else {
            start = -1;
        }
    }

    private void advice() {
        if (matcher.find()) {
            end = matcher.start();
        } else {
            end = length;
        }
    }

    @Override
    public boolean empty() {
        return start < 0;
    }

    @Override
    public void setFront(final CharSequence v) {
        throw new UnsupportedOperationException();
    }
}
