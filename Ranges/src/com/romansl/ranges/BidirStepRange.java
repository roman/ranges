package com.romansl.ranges;

public class BidirStepRange<T> extends BidirRange<T> {
    private final BidirRange<T> range;
    private final int step;

    public BidirStepRange(final BidirRange<T> range, final int step) {
        this.range = range;
        this.step = step;
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public T back() {
        return range.back();
    }

    @Override
    public void popFront() {
        range.popFront(step);
    }

    @Override
    public void popBack() {
        range.popBack(step);
    }

    @Override
    public void popFront(final int n) {
        range.popFront(step * n);
    }

    @Override
    public void popBack(final int n) {
        range.popBack(step * n);
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }

    @Override
    public void setBack(final T v) {
        range.setBack(v);
    }
}
