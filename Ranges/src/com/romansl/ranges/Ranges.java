package com.romansl.ranges;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ranges {
    private static final EmptyRange EMPTY_RANGE = new EmptyRange();

    public static BidirIntegerRange counter2(final int from, final int to) {
        if (from < to)
            return new BidirIntegerRange(from, to);
        else
            throw new IllegalArgumentException("from=" + from + " to=" + to);
    }

    public static IntegerRange counter(final int from, final int to) {
        if (from < to)
            return new IntegerRange(from, to);
        else
            throw new IllegalArgumentException("from=" + from + " to=" + to);
    }

    public static <T, R extends BidirRange<T>> ReverseRange<T, R> reverse(final R range) {
        return new ReverseRange<T, R>(range);
    }

    public static <T, R extends BidirRange<T>> R reverse(final ReverseRange<T, R> range) {
        return range.range; // reverse(reverse(r)) == r
    }

    public static BidirCharacterRange range(final CharSequence chars) {
        return new BidirCharacterRange(chars);
    }

    public static BidirCharacterRange range2(final CharSequence chars) {
        return new BidirCharacterRange(chars);
    }

    public static <T> BidirArrayRange<T> range2(final T[] array) {
        return new BidirArrayRange<T>(array);
    }

    public static <T> ArrayRange<T> range(final T[] array) {
        return new ArrayRange<T>(array);
    }

    public static FloatArrayRange range(final float[] array) {
        return new FloatArrayRange(array);
    }

    public static BidirFloatArrayRange range2(final float[] array) {
        return new BidirFloatArrayRange(array);
    }

    public static IntegerArrayRange range(final int[] array) {
        return new IntegerArrayRange(array);
    }

    public static BidirIntegerArrayRange range2(final int[] array) {
        return new BidirIntegerArrayRange(array);
    }

    public static <T> BidirArrayRange<T> range(final T[] array, final int offset, final int length) {
        return new BidirArrayRange<T>(array, offset, length);
    }

    public static <T> BidirArrayRange<T> range2(final T[] array, final int offset, final int length) {
        return new BidirArrayRange<T>(array, offset, length);
    }

    public static <T> Range<T> range(final Iterable<T> iterable)  {
        return new IteratorRange<T>(iterable.iterator());
    }

    public static <T> Range<T> range(final Iterator<T> iterator)  {
        return new IteratorRange<T>(iterator);
    }

    public static <T> BidirListRange<T> range(final List<T> list)  {
        return new BidirListRange<T>(list);
    }

    public static <T> BidirListRange<T> range2(final List<T> list)  {
        return new BidirListRange<T>(list);
    }

    public static <A, B> BidirZipRange<A, B> zip(final BidirRange<A> a, final BidirRange<B> b) {
        return new BidirZipRange<A, B>(b, a);
    }

    public static <A, B> ZipRange<A, B> zip(final Range<A> a, final Range<B> b) {
        return new ZipRange<A, B>(a, b);
    }

    public static <T> FilterRange<T> filter(final Range<T> range, final Predicate<? super T> predicate) {
        return new FilterRange<T>(range, predicate);
    }

    public static <T> BidirFilterRange<T> filter(final BidirRange<T> range, final Predicate<? super T> predicate) {
        return new BidirFilterRange<T>(range, predicate);
    }

    public static BidirInfiniteIntegerRange infinite2(final int from) {
        return new BidirInfiniteIntegerRange(from);
    }

    public static InfiniteIntegerRange infinite(final int from) {
        return new InfiniteIntegerRange(from);
    }

    public static BidirInfiniteRandomIntegerRange randomIntRange2(final int maxRandomValue) {
        return new BidirInfiniteRandomIntegerRange(maxRandomValue);
    }

    public static InfiniteRandomIntegerRange randomIntRange(final int maxRandomValue) {
        return new InfiniteRandomIntegerRange(maxRandomValue);
    }

    public static <T, R extends Range<? extends T>> R find(final R range, final T value) {
        if (value == null) {
            for (; !range.empty(); range.popFront()) {
                if (range.front() == null) {
                    break;
                }
            }
        } else {
            for (; !range.empty(); range.popFront()) {
                if (value.equals(range.front())) {
                    break;
                }
            }
        }

        return range;
    }

    public static <T, R extends Range<? extends T>, P extends Predicate<? super T>> R find(final R range, final P predicate) {
        if (predicate == null)
            throw new NullPointerException("predicate is null! Maybe you mean find(range, (T) null)?");

        for (; !range.empty(); range.popFront()) {
            if (predicate.apply(range.front())) {
                break;
            }
        }

        return range;
    }

    public static <T> int indexOf(final Range<T> range, final Predicate<? super T> predicate) {
        if (predicate == null)
            throw new NullPointerException("predicate is null! Maybe you mean indexOf(range, (T) null)?");

        for (int i = 0; !range.empty(); range.popFront(), i++) {
            if (predicate.apply(range.front()))
                return i;
        }

        return -1;
    }

    public static <T, V extends T> int indexOf(final Range<T> range, final V value) {
        if (value == null) {
            for (int i = 0; !range.empty(); range.popFront(), i++) {
                if (range.front() == null)
                    return i;
            }
        } else {
            for (int i = 0; !range.empty(); range.popFront(), i++) {
                if (value.equals(range.front()))
                    return i;
            }
        }

        return -1;
    }

    public static <T> int indexOf(final Iterable<T> iterable, final Predicate<? super T> predicate) {
        // TODO: optimize
        return indexOf(range(iterable), predicate);
    }

    public static <T, V extends T> int indexOf(final Iterable<T> iterable, final V value) {
        // TODO: optimize
        return indexOf(range(iterable), value);
    }

    public static <T> int indexOf(final List<T> collection, final Predicate<? super T> predicate) {
        // TODO: optimize
        return indexOf(range(collection), predicate);
    }

    public static <T, V extends T> int indexOf(final List<T> collection, final V value) {
        // TODO: optimize
        return indexOf(range(collection), value);
    }

    public static <T> int indexOf(final T[] array, final Predicate<? super T> predicate) {
        // TODO: optimize
        return indexOf(range(array), predicate);
    }

    public static int indexOf(final int[] range, final int value) {
        // TODO: optimize
        return indexOf(range(range), value);
    }

    public static <T, V extends T> int indexOf(final T[] range, final V value) {
        // TODO: optimize
        return indexOf(range(range), value);
    }

    public static <T> boolean contains(final Range<? extends T> range, final T value) {
        // TODO: optimize
        return !find(range, value).empty();
    }

    public static <T> boolean contains(final Range<? extends T> range, final Predicate<? super T> predicate) {
        // TODO: optimize
        return !find(range, predicate).empty();
    }

    public static <T> boolean contains(final T[] array, final Predicate<? super T> predicate) {
        for (final T v : array) {
            if (predicate.apply(v)) {
                return true;
            }
        }

        return false;
    }

    public static <T> boolean contains(final Collection<T> collection, final T value) {
        // TODO: optimize
        return !find(range(collection), value).empty();
    }

    public static <T> boolean contains(final Collection<T> collection, final Predicate<? super T> predicate) {
        // TODO: optimize
        return !find(range(collection), predicate).empty();
    }

    public static <T> boolean contains(final T[] array, final T value) {
        if (value == null) {
            for (final T v : array) {
                if (v == null) {
                    return true;
                }
            }
        } else {
            for (final T v : array) {
                if (value.equals(v)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static <T> ArrayList<T> toList(final Range<T> range) {
        final ArrayList<T> al = new ArrayList<T>();

        for (; !range.empty(); range.popFront()) {
            al.add(range.front());
        }

        return al;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(final Range<T> range, final Class<T> dstType) {
        final ArrayList<T> al = new ArrayList<T>();

        for (; !range.empty(); range.popFront()) {
            al.add(range.front());
        }

        return al.toArray((T[]) Array.newInstance(dstType, al.size()));
    }

    /**
     * Copy r2 to r1.
     *
     * @param to r1
     * @param from r2
     * @return Untouched portion of r1.
     */
    public static <T, R extends Range<T>> R copy(final R to, final Range<? extends T> from) {
        for (; !to.empty() && !from.empty(); to.popFront(), from.popFront()) {
            to.setFront(from.front());
        }

        return to;
    }

    public static <T> ArrayRange<T> copy(final T[] to, final Range<? extends T> from) {
        final int n = to.length;
        int i = 0;

        for (; i < n && !from.empty(); from.popFront(), i++) {
            to[i] = from.front();
        }

        return new ArrayRange<T>(to, i);
    }

    public static <T, R extends Range<T>> R copy(final R to, final T[] from) {
        final int n = from.length;

        for (int i = 0; i < n && !to.empty(); to.popFront(), i++) {
            to.setFront(from[i]);
        }

        return to;
    }

    /**
     * Experimental.
     * fix: copy reverse array to itself.
     */
    public static <T, R extends BidirRange<T>> R bicopy(final R to, final BidirRange<? extends T> from) {
        for (; !to.empty() && !from.empty(); to.popFront(), to.popBack(), from.popFront(), from.popBack()) {
            final T front = from.front();
            final T back = from.back();
            to.setFront(front);
            to.setBack(back);
        }

        return to;
    }

    public static <T, R extends Range<T>> R swap(final R r1, final Range<T> r2) {
        for (;!r1.empty() && !r2.empty(); r1.popFront(), r2.popFront()) {
            final T tmp = r1.front();
            r1.setFront(r2.front());
            r2.setFront(tmp);
        }

        return r1;
    }

    public static <T> GenericChainRange<T> chain(final Range<T>... ranges) {
        return new GenericChainRange<T>(ranges);
    }

    public static <T, R extends Range<T>> GenericChainRangeOfRanges<T, R> chain(final Range<R> range) {
        return new GenericChainRangeOfRanges<T, R>(range);
    }

    public static <T> ChainRange2<T> chain(final Range<T> r1, final Range<T> r2) {
        return new ChainRange2<T>(r1, r2);
    }

    public static <T> InfiniteRepeatValueRange<T> repeat(final T v) {
        return new InfiniteRepeatValueRange<T>(v);
    }

    /**
     * Equals <code>take(repeat(v), n);</code>
     */
    public static <T> RepeatValueRange<T> repeat(final T v, final int n) {
        return new RepeatValueRange<T>(v, n);
    }

    public static <T> boolean equals(final Range<? extends T> r1, final Range<? extends T> r2) {
        for (;;) {
            if (r1.empty())
                return r2.empty();
            else if (r2.empty())
                return false;

            final T v1 = r1.front();
            final T v2 = r2.front();
            final boolean eq = v1 == null ? v2 == null : v1.equals(v2);
            if (!eq)
                return false;

            r1.popFront();
            r2.popFront();
        }
    }

    public static <T> TakeRange<T> take(final int n, final Range<T> range) {
        return new TakeRange<T>(range, n);
    }

    public static <T> BidirTakeRange<T> take(final int n, final BidirRange<T> range) {
        return new BidirTakeRange<T>(range, n);
    }

    public static <T extends Comparable<? super T>, R extends Range<T>> BidirCastToTypeArrayRange<T> sort(final R range) {
        final ArrayList<T> list = toList(range);
        final Object[] a = list.toArray();
        Arrays.sort(a);
        return new BidirCastToTypeArrayRange<T>(a);
    }

    public static <A extends Appendable, T> A append(final A appendable, final Range<T> range) {
        try {
            for(; !range.empty(); range.popFront()) {
                appendable.append(range.front().toString());
            }
        } catch (final IOException impossible) {
            throw new AssertionError(impossible);
        }

        return appendable;
    }


    public static <C extends Collection<T>, T> C append(final C to, final Range<? extends T> range) {
        for(; !range.empty(); range.popFront()) {
            to.add(range.front());
        }

        return to;
    }

    /**
     * @param r1 Example: [a, b, c]
     * @param r2 Example: [A, B, C]
     * @return Example: [a, A, b, B, c, C]
     */
    public static <T> InterchangeRange<T> interchange(final Range<T> r1, final Range<T> r2){
        return new InterchangeRange<T>(r1, r2);
    }

    public static <T> GeneratorRange<T> generate(final Generator<T> generator) {
        return new GeneratorRange<T>(generator);
    }

    public static <R, T> TransformRange<R, T> transform(final Range<T> range, final Function<? super T, ? extends R> function) {
        return new TransformRange<R, T>(range, function);
    }

    public static <R, T> TransformRange<R, T> map(final Range<T> range, final Function<? super T, ? extends R> function) {
        return new TransformRange<R, T>(range, function);
    }

    public static <R, T> BidirTransformRange<R, T> transform(final BidirRange<T> range, final Function<? super T, ? extends R> function) {
        return new BidirTransformRange<R, T>(range, function);
    }

    public static <R, T> BidirTransformRange<R, T> map(final BidirRange<T> range, final Function<? super T, ? extends R> function) {
        return new BidirTransformRange<R, T>(range, function);
    }

    /**
     * Returns a string representation of {@code range}, with the format {@code [e1, e2, ..., en]}.
     */
    public static String toString(final Range<?> range) {
        if (range.empty())
            return "[]";

        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        doJoin(builder, range, ", ");
        return builder.append(']').toString();
    }

    public static StringBuilder join(final StringBuilder builder, final Range<?> range, final String separator) {
        if (!range.empty()) {
            doJoin(builder, range, separator);
        }

        return builder;
    }

    private static void doJoin(final StringBuilder builder, final Range<?> range, final String separator) {
        builder.append(range.front());
        range.popFront();

        for (; !range.empty(); range.popFront()) {
            builder.append(separator).append(range.front());
        }
    }

    public static StringBuilder join(final Range<?> range, final String separator) {
        return join(new StringBuilder(), range, separator);
    }

    /**
     * @return the number of occurrences of value in range.
     */
    public static <T, V extends T> int countOf(final Range<T> range, final V value) {
        int counter = 0;

        if (value == null) {
            for(; !range.empty(); range.popFront()) {
                if (range.front() == null) {
                    counter++;
                }
            }
        } else {
            for(; !range.empty(); range.popFront()) {
                if (value.equals(range.front())) {
                    counter++;
                }
            }
        }

        return counter;
    }

    /**
     * @return the number of occurrences of value in array.
     */
    public static int countOf(final Object[] array, final Object value) {
        int counter = 0;

        if (value == null) {
            for (final Object v : array) {
                if (v == null) {
                    counter++;
                }
            }
        } else {
            for (final Object v : array) {
                if (value.equals(v)) {
                    counter++;
                }
            }
        }

        return counter;
    }

    public static int countOf(final int[] array, final int value) {
        int counter = 0;

        for (final int v : array) {
            if (value == v) {
                counter++;
            }
        }

        return counter;
    }

    public static int countOf(final boolean[] array, final boolean value) {
        int counter = 0;

        for (final boolean v : array) {
            if (value == v) {
                counter++;
            }
        }

        return counter;
    }

    public static int countOf(final long[] array, final long value) {
        int counter = 0;

        for (final long v : array) {
            if (value == v) {
                counter++;
            }
        }

        return counter;
    }

    public static <T> int countOf(final Range<T> range, final Predicate<? super T> predicate) {
        if (predicate == null)
            throw new NullPointerException("predicate is null! Maybe you mean countOf(range, (T) null)?");

        int counter = 0;

        for(; !range.empty(); range.popFront()) {
            if (predicate.apply(range.front())) {
                ++counter;
            }
        }

        return counter;
    }

    public static <T> int countOf(final T[] array, final Predicate<? super T> predicate) {
        if (predicate == null)
            throw new NullPointerException("predicate is null! Maybe you mean countOf(range, (T) null)?");

        int counter = 0;

        for(final T item : array) {
            if (predicate.apply(item)) {
                ++counter;
            }
        }

        return counter;
    }

    public static <P1, P2> P1 reduce(final Range<P2> range, final BinaryFunction<P1, P2, P1> function, P1 initial) {
        for(; !range.empty(); range.popFront()) {
            initial = function.apply(initial, range.front());
        }

        return initial;
    }

    public static <T> StepRange<T> step(final Range<T> range, final int step) {
        return new StepRange<T>(range, step);
    }

    public static <T> BidirStepRange<T> step(final BidirRange<T> range, final int step) {
        return new BidirStepRange<T>(range, step);
    }

    public static <T extends Comparable<T>> T max(final Range<T> range) {
        if (range.empty())
            return null;

        T a = range.front();

        for(range.popFront(); !range.empty(); range.popFront()) {
            final T b = range.front();
            final int c = a.compareTo(b);
            if (c < 0) {
                a = b;
            }
        }

        return a;
    }

    public static <T extends Comparable<T>> T min(final Range<T> range) {
        if (range.empty())
            return null;

        T a = range.front();

        for(range.popFront(); !range.empty(); range.popFront()) {
            final T b = range.front();
            final int c = a.compareTo(b);
            if (c > 0) {
                a = b;
            }
        }

        return a;
    }

    public static RegexMatchRange<CharSequence> split(final CharSequence input, final Matcher matcher) {
        if (input == null || matcher == null)
            throw new NullPointerException();

        return new RegexMatchRange<CharSequence>(input, matcher) {
            @Override
            public CharSequence front() {
                return input.subSequence(start, end);
            }
        };
    }

    public static RegexMatchRange<CharSequence> split(final CharSequence input, final Pattern pattern) {
        return split(input, pattern.matcher(input));
    }

    public static RegexMatchRange<CharSequence> split(final CharSequence input, final String pattern) {
        return split(input, Pattern.compile(pattern).matcher(input));
    }

    public static RegexMatchRange<String> split(final String input, final Matcher matcher) {
        if (input == null || matcher == null)
            throw new NullPointerException();

        return new RegexMatchRange<String>(input, matcher) {
            @Override
            public String front() {
                return input.substring(start, end);
            }
        };
    }

    public static RegexMatchRange<String> split(final String input, final Pattern pattern) {
        return split(input, pattern.matcher(input));
    }

    public static RegexMatchRange<String> split(final String input, final String pattern) {
        return split(input, Pattern.compile(pattern).matcher(input));
    }

    public static <T> EmptyRange<T> emptyRange() {
        //noinspection unchecked
        return EMPTY_RANGE;
    }

    public static <D, S> CastRange<D, S> cast(final Range<S> src) {
        return new CastRange<D, S>(src);
    }
}
