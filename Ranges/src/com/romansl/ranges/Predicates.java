package com.romansl.ranges;

public class Predicates {
    public static final Predicate<Integer> IS_EVEN = new Predicate<Integer>() {
        @Override
        public boolean apply(final Integer input) {
            return input % 2 == 0;
        }
    };
    public static final Predicate<Object> IS_NULL = new Predicate<Object>() {
        @Override
        public boolean apply(final Object input) {
            return input == null;
        }
    };
    public static final Predicate<Object> IS_NOT_NULL = new Predicate<Object>() {
        @Override
        public boolean apply(final Object input) {
            return input != null;
        }
    };
    public static final Predicate<Object> FALSE = new Predicate<Object>() {
        @Override
        public boolean apply(final Object input) {
            return false;
        }
    };
    public static final Predicate<Object> TRUE = new Predicate<Object>() {
        @Override
        public boolean apply(final Object input) {
            return true;
        }
    };

    public static <T> Predicate<T> not(final Predicate<T> p) {
        return new Predicate<T>() {
            @Override
            public boolean apply(final T input) {
                return !p.apply(input);
            }
        };
    }

    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> equalsTo(final T value) {
        if (value == null)
            return (Predicate<T>) IS_NULL;
        else
            return new Predicate<T>() {
            @Override
            public boolean apply(final T input) {
                return value.equals(input);
            }
        };
    }

    public static Predicate<Object> instanceOf(final Class c) {
        return new Predicate<Object>() {
            @Override
            public boolean apply(final Object input) {
                return c.isInstance(input);
            }
        };
    }
}
