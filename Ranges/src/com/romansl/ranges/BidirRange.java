package com.romansl.ranges;

public abstract class BidirRange<T> extends Range<T> {
    public abstract T back();
    public abstract void popBack();
    public abstract void setBack(T v);

    public void popBack(int n) {
        for(; n > 0; --n) {
            popBack();
        }
    }

    public T backOrDefault(final T defaultValue) {
        if (empty()) {
            return defaultValue;
        } else {
            return back();
        }
    }

    public T backOrNull() {
        if (empty()) {
            return null;
        } else {
            return back();
        }
    }
}
