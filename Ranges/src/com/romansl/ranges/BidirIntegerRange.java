package com.romansl.ranges;

public class BidirIntegerRange extends BidirRange<Integer> {
    private int front;
    private int back;

    public BidirIntegerRange(final int from, final int to) {
        front = from;
        back = to;
    }

    @Override
    public Integer front() {
        return front;
    }

    @Override
    public Integer back() {
        return back;
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

   @Override
    public void setFront(final Integer v) {
        // no action
    }

    @Override
    public void setBack(final Integer v) {
        // no action
    }
}
