package com.romansl.ranges;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Range<T> implements Iterable<T> {
    public abstract T front();
    public abstract void popFront();
    public abstract boolean empty();
    public abstract void setFront(final T v);

    @Override
    public Iterator<T> iterator() {
        return new RangeIterator<T>(this);
    }

    public void popFront(int n) {
        for(; n > 0; --n) {
            popFront();
        }
    }

    public FilterRange<T> filter(final Predicate<? super T> predicate) {
        return Ranges.filter(this, predicate);
    }

    public Range<T> find(final T value) {
        return Ranges.find(this, value);
    }

    public Range<T> find(final Predicate<? super T> value) {
        return Ranges.find(this, value);
    }

    public int indexOf(final T value) {
        return Ranges.indexOf(this, value);
    }

    public int indexOf(final Predicate<? super T> predicate) {
        return Ranges.indexOf(this, predicate);
    }

    public boolean contains(final T value) {
        return Ranges.contains(this, value);
    }

    public boolean contains(final Predicate<? super T> predicate) {
        return Ranges.contains(this, predicate);
    }

    public ArrayList<T> toList() {
        return Ranges.toList(this);
    }

    public T[] toArray(final Class<T> dstType) {
        return Ranges.toArray(this, dstType);
    }

    public ChainRange2<T> chain(final Range<T> range) {
        return Ranges.chain(this, range);
    }

    public <R> TransformRange<R, T> map(final Function<? super T, ? extends R> function) {
        return Ranges.transform(this, function);
    }

    public <P> P reduce(final BinaryFunction<P, T, P> function, P initial) {
        for(; !empty(); popFront()) {
            initial = function.apply(initial, front());
        }

        return initial;
    }

    public TakeRange<T> take(final int n) {
        return new TakeRange<T>(this, n);
    }

    public Range<T> skip(final int n) {
        popFront(n);
        return this;
    }

    public Range<T> step(final int n) {
        return new StepRange<T>(this, n);
    }

    public StringBuilder join(final String separator) {
        return Ranges.join(this, separator);
    }

    public T frontOrDefault(final T defaultValue) {
        if (empty()) {
            return defaultValue;
        } else {
            return front();
        }
    }

    public T frontOrNull() {
        if (empty()) {
            return null;
        } else {
            return front();
        }
    }

    public <D> CastRange<D, T> cast() {
        return new CastRange<D, T>(this);
    }
}
