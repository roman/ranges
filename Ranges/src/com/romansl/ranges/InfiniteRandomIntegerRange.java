package com.romansl.ranges;

import java.util.Random;

public class InfiniteRandomIntegerRange extends Range<Integer> {
    private final int maxRandomValue;
    private final Random rnd = new Random();
    private int front;

    public InfiniteRandomIntegerRange(final int maxRandomValue) {
        this.maxRandomValue = maxRandomValue;
        front = rnd.nextInt(maxRandomValue);
    }

    @Override
    public Integer front() {
        return front;
    }

    @Override
    public void popFront() {
        front = rnd.nextInt(maxRandomValue);
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final Integer v) {
        // no action
    }
}
