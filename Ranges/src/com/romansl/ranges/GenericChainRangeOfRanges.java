package com.romansl.ranges;

public class GenericChainRangeOfRanges<T, R extends Range<T>> extends Range<T> {
    private final Range<R> range;

    public GenericChainRangeOfRanges(final Range<R> range) {
        this.range = range;
    }

    @Override
    public T front() {
        return range.front().front();
    }

    @Override
    public void popFront() {
        range.front().popFront();
    }

    @Override
    public boolean empty() {
        while (!range.empty()) {
            if (range.front().empty()) {
                range.popFront();
            } else {
                return false;
            }
        }

        return true;
    }

    @Override
    public void setFront(final T v) {
        range.front().setFront(v);
    }
}
