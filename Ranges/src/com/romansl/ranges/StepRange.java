package com.romansl.ranges;

public class StepRange<T> extends Range<T> {
    private final Range<T> range;
    private final int step;

    public StepRange(final Range<T> range, final int step) {
        this.range = range;
        this.step = step;
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public void popFront() {
        range.popFront(step);
    }

    @Override
    public void popFront(final int n) {
        range.popFront(step * n);
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }
}
