package com.romansl.ranges;

public class GeneratorRange<T> extends Range<T> {
    private final Generator<T> generator;
    private T generated;

    public GeneratorRange(final Generator<T> generator) {
        this.generator = generator;
        generated = generator.generate();
    }

    @Override
    public T front() {
        return generated;
    }

    @Override
    public void popFront() {
        generated = generator.generate();
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final T v) {
        generated = v;
    }
}
