package com.romansl.ranges;

public class BidirTakeRange<T> extends BidirRange<T> {
    private final BidirRange<T> range;
    private int i;

    public BidirTakeRange(final BidirRange<T> range, final int n) {
        this.range = range;
        i = n;
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public T back() {
        return range.back();
    }

    @Override
    public void popFront() {
        range.popFront();
        i--;
    }

    @Override
    public void popBack() {
        range.popBack();
        i--;
    }

    @Override
    public boolean empty() {
        return range.empty() || i <= 0;
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }

    @Override
    public void setBack(final T v) {
        range.setBack(v);
    }
}
