package com.romansl.ranges;

public class IntegerArrayRange extends Range<Integer> {
    private final int[] array;
    private int front;

    public IntegerArrayRange(final int[] array) {
        this.array = array;
    }

    public IntegerArrayRange(final int[] array, final int front) {
        this.array = array;
        this.front = front;
    }

    @Override
    public Integer front() {
        return array[front];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public boolean empty() {
        return front >= array.length;
    }

    @Override
    public void setFront(final Integer v) {
        array[front] = v;
    }
}
