package com.romansl.ranges;

public interface BinaryFunction<Param1, Param2, Result> {
    Result apply(Param1 p1, Param2 p2);
}