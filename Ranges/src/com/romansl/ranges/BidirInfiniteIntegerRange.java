package com.romansl.ranges;

public class BidirInfiniteIntegerRange extends BidirRange<Integer> {
    private int front;
    private int back;

    public BidirInfiniteIntegerRange(final int from) {
        front = from;
        back = from;
    }

    @Override
    public Integer front() {
        return front;
    }

    @Override
    public Integer back() {
        return back;
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final Integer v) {
        // no action
    }

    @Override
    public void setBack(final Integer v) {
        // no action
    }
}
