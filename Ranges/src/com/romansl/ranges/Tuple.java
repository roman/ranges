package com.romansl.ranges;

public class Tuple<A, B> {
    private A a;
    private B b;

    public Tuple() {}

    public Tuple(final Tuple<A, B> src) {
        a = src.a;
        b = src.b;
    }

    public Tuple(final A a, final B b) {
        this.a = a;
        this.b = b;
    }

    public void set(final A a, final B b) {
        this.a = a;
        this.b = b;
    }

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

    @Override
    public String toString() {
        return "Tuple(" + a + ", " + b + ")";
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this)
            return true;

        if (obj instanceof Tuple<?, ?>) {
            final Tuple<?, ?> tuple = (Tuple<?, ?>) obj;
            return (a == null ? tuple.a == null : a.equals(tuple.a))
                && (b == null ? tuple.b == null : b.equals(tuple.b));
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 31 + (a == null ? 0 : a.hashCode());
        hash = hash * 31 + (b == null ? 0 : b.hashCode());
        return hash;
    }
}

