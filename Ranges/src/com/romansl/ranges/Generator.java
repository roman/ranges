package com.romansl.ranges;

public interface Generator<T> {
    T generate();
}