package com.romansl.ranges;

public class FloatArrayRange extends Range<Float> {
    private final float[] array;
    private int front;

    public FloatArrayRange(final float[] array) {
        this.array = array;
    }

    public FloatArrayRange(final float[] array, final int front) {
        this.array = array;
        this.front = front;
    }

    @Override
    public Float front() {
        return array[front];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public boolean empty() {
        return front >= array.length;
    }

    @Override
    public void setFront(final Float v) {
        array[front] = v;
    }
}
