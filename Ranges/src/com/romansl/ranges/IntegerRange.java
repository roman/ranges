package com.romansl.ranges;

public class IntegerRange extends Range<Integer> {
    private int front;
    private final int back;

    public IntegerRange(final int from, final int to) {
        front = from;
        back = to;
    }

    @Override
    public Integer front() {
        return front;
    }

    @Override
    public void popFront() {
        ++front;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

   @Override
    public void setFront(final Integer v) {
        // no action
    }
}
