package com.romansl.ranges;

import java.util.Iterator;
import java.util.List;

public class BidirListRange<T> extends BidirRange<T> implements Iterator<T> {
    private final List<T> list;
    private int front;
    private int back;

    public BidirListRange(final List<T> list) {
        this.list = list;
        this.front = 0;
        back = list.size() - 1;
    }

    public BidirListRange(final List<T> list, final int front, final int back) {
        this.list = list;
        this.front = front;
        this.back = back;
    }

    @Override
    public T front() {
        return list.get(front);
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final T v) {
        list.set(front, v);
    }

    @Override
    public T back() {
        return list.get(back);
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public void setBack(final T v) {
        list.set(back, v);
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return front <= back;
    }

    @Override
    public T next() {
        return list.get(front++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }
}
