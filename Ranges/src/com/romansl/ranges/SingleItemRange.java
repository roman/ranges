package com.romansl.ranges;

public class SingleItemRange<T> extends BidirRange<T> {
    private T mItem;
    private boolean mIsEmpty;

    public SingleItemRange(final T item) {
        mItem = item;
    }

    @Override
    public T back() {
        return mItem;
    }

    @Override
    public void popBack() {
        mIsEmpty = true;
    }

    @Override
    public void setBack(final T v) {
        mItem = v;
    }

    @Override
    public T front() {
        return mItem;
    }

    @Override
    public void popFront() {
        mIsEmpty = true;
    }

    @Override
    public boolean empty() {
        return mIsEmpty;
    }

    @Override
    public void setFront(final T v) {
        mItem = v;
    }
}
