package com.romansl.ranges;

public interface Predicate<T> {
    boolean apply(T input);
}
