package com.romansl.ranges;

public class TransformRange<Result, Param> extends Range<Result> {
    private final Range<Param> range;
    private final Function<? super Param, ? extends Result> function;

    public TransformRange(final Range<Param> range, final Function<? super Param, ? extends Result> function) {
        this.range = range;
        this.function = function;
    }

    @Override
    public Result front() {
        return function.apply(range.front());
    }

    @Override
    public void popFront() {
        range.popFront();
    }

    @Override
    public void popFront(final int n) {
        range.popFront(n);
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final Result v) {
        throw new UnsupportedOperationException();
    }
}
