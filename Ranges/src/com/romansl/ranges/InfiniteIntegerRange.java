package com.romansl.ranges;

public class InfiniteIntegerRange extends Range<Integer> {
    private int front;

    public InfiniteIntegerRange(final int from) {
        front = from;
    }

    @Override
    public Integer front() {
        return front;
    }


    @Override
    public void popFront() {
        front++;
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final Integer v) {
        // no action
    }
}
