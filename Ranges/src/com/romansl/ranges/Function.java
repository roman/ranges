package com.romansl.ranges;

public interface Function<Param, Result> {
    Result apply(Param p);
}