package com.romansl.ranges;

public class ZipRange<A, B> extends Range<Tuple<A, B>> {
    private final Range<A> a;
    private final Range<B> b;

    public ZipRange(final Range<A> a, final Range<B> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public Tuple<A, B> front() {
        return new Tuple<A, B>(a.front(), b.front());
    }

    @Override
    public void popFront() {
        a.popFront();
        b.popFront();
    }

    @Override
    public boolean empty() {
        return a.empty() || b.empty();
    }

    @Override
    public void setFront(final Tuple<A, B> v) {
        a.setFront(v.getA());
        b.setFront(v.getB());
    }
}
