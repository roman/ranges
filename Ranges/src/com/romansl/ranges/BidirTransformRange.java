package com.romansl.ranges;

public class BidirTransformRange<Result, Param> extends BidirRange<Result> {
    private final BidirRange<Param> range;
    private final Function<? super Param, ? extends Result> function;

    public BidirTransformRange(final BidirRange<Param> range, final Function<? super Param, ? extends Result> function) {
        this.range = range;
        this.function = function;
    }

    @Override
    public Result front() {
        return function.apply(range.front());
    }

    @Override
    public Result back() {
        return function.apply(range.back());
    }

    @Override
    public void popFront() {
        range.popFront();
    }

    @Override
    public void popBack() {
        range.popBack();
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final Result v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBack(final Result v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void popBack(final int n) {
        range.popBack(n);
    }

    @Override
    public void popFront(final int n) {
        range.popFront(n);
    }
}
