package com.romansl.ranges;

public class TakeRange<T> extends Range<T> {
    private final Range<T> range;
    private int i;

    public TakeRange(final Range<T> range, final int n) {
        this.range = range;
        i = n;
    }

    @Override
    public T front() {
        return range.front();
    }

    @Override
    public void popFront() {
        range.popFront();
        i--;
    }

    @Override
    public boolean empty() {
        return range.empty() || i <= 0;
    }

    @Override
    public void setFront(final T v) {
        range.setFront(v);
    }
}
