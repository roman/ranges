package com.romansl.ranges;

public class BidirZipRange<A, B> extends BidirRange<Tuple<A, B>> {
    private final BidirRange<B> b;
    private final BidirRange<A> a;

    public BidirZipRange(final BidirRange<B> b, final BidirRange<A> a) {
        this.b = b;
        this.a = a;
    }

    @Override
    public Tuple<A, B> front() {
        return new Tuple<A, B>(a.front(), b.front());
    }

    @Override
    public Tuple<A, B> back() {
        return new Tuple<A, B>(a.back(), b.back());
    }

    @Override
    public void popFront() {
        a.popFront();
        b.popFront();
    }

    @Override
    public void popBack() {
        a.popBack();
        b.popBack();
    }

    @Override
    public boolean empty() {
        return a.empty() || b.empty();
    }

    @Override
    public void setFront(final Tuple<A, B> src) {
        a.setFront(src.getA());
        b.setFront(src.getB());
    }

    @Override
    public void setBack(final Tuple<A, B> src) {
        a.setBack(src.getA());
        b.setBack(src.getB());
    }
}
