package com.romansl.ranges;

import java.util.Iterator;

public class BidirCastToTypeArrayRange<T> extends BidirRange<T> {
    private final Object[] array;
    private int front;
    private int back;

    public BidirCastToTypeArrayRange(final Object[] array) {
        this.array = array;
        front = 0;
        back = array.length - 1;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T front() {
        return (T) array[front];
    }

    @SuppressWarnings("unchecked")
    @Override
    public T back() {
        return (T) array[back];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final T v) {
        array[front] = v;
    }

    @Override
    public void setBack(final T v) {
        array[back] = v;
    }

    @Override
    public void popFront(final int n) {
        front += n;
    }

    @Override
    public void popBack(final int n) {
        back -= n;
    }
}
