package com.romansl.ranges;

public class InfiniteRepeatValueRange<T> extends Range<T> {
    private final T v;

    InfiniteRepeatValueRange(final T v) {
        this.v = v;
    }

    @Override
    public T front() {
        return v;
    }

    @Override
    public void popFront() {
        // no action
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final T v) {
        // no action
    }
}
