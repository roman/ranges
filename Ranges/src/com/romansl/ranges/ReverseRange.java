package com.romansl.ranges;

public class ReverseRange<T, R extends BidirRange<T>> extends BidirRange<T> {
    final R range;

    public ReverseRange(final R range) {
        this.range = range;
    }

    @Override
    public T front() {
        return range.back();
    }

    @Override
    public T back() {
        return range.front();
    }

    @Override
    public void popFront() {
        range.popBack();
    }

    @Override
    public void popBack() {
        range.popFront();
    }

    @Override
    public boolean empty() {
        return range.empty();
    }

    @Override
    public void setFront(final T v) {
        range.setBack(v);
    }

    @Override
    public void setBack(final T v) {
        range.setFront(v);
    }

    @Override
    public void popBack(final int n) {
        range.popFront(n);
    }

    @Override
    public void popFront(final int n) {
        range.popBack(n);
    }
}
