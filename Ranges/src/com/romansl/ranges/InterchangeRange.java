package com.romansl.ranges;

public class InterchangeRange<T> extends Range<T> {
    private final Range<T> r2;
    private final Range<T> r1;
    private Range<T> current;

    public InterchangeRange(final Range<T> r1, final Range<T> r2) {
        this.r2 = r2;
        this.r1 = r1;
        current = r1;
    }

    @Override
    public T front() {
        return current.front();
    }

    @Override
    public void popFront() {
        current.popFront();

        if (current == r1) {
            current = r2;
        } else {
            current = r1;
        }
    }

    @Override
    public boolean empty() {
        return current.empty();
    }

    @Override
    public void setFront(final T v) {
        current.setFront(v);
    }
}
