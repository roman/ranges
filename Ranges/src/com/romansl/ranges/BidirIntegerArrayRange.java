package com.romansl.ranges;

public class BidirIntegerArrayRange extends BidirRange<Integer> {
    private final int[] array;
    private int front;
    private int back;

    public BidirIntegerArrayRange(final int[] array) {
        this.array = array;
        front = 0;
        back = array.length - 1;
    }

    public BidirIntegerArrayRange(final int[] array, final int offset, final int length) {
        this.array = array;
        front = offset;
        back = offset + length - 1;
    }

    @Override
    public Integer front() {
        return array[front];
    }

    @Override
    public Integer back() {
        return array[back];
    }

    @Override
    public void popFront() {
        front++;
    }

    @Override
    public void popBack() {
        back--;
    }

    @Override
    public boolean empty() {
        return front > back;
    }

    @Override
    public void setFront(final Integer v) {
        array[front] = v;
    }

    @Override
    public void setBack(final Integer v) {
        array[back] = v;
    }
}
