package com.romansl.ranges;

import java.util.Random;

public class BidirInfiniteRandomIntegerRange extends BidirRange<Integer> {
    private final int maxRandomValue;
    private final Random rnd = new Random();
    private int front;
    private int back;

    public BidirInfiniteRandomIntegerRange(final int maxRandomValue) {
        this.maxRandomValue = maxRandomValue;
        front = rnd.nextInt(maxRandomValue);
        back = rnd.nextInt(maxRandomValue);
    }

    @Override
    public Integer front() {
        return front;
    }

    @Override
    public Integer back() {
        return back;
    }

    @Override
    public void popFront() {
        front = rnd.nextInt(maxRandomValue);
    }

    @Override
    public void popBack() {
        back = rnd.nextInt(maxRandomValue);
    }

    @Override
    public void popFront(final int n) {
        front = rnd.nextInt(maxRandomValue);
    }

    @Override
    public void popBack(final int n) {
        back = rnd.nextInt(maxRandomValue);
    }

    @Override
    public boolean empty() {
        return false;
    }

    @Override
    public void setFront(final Integer v) {
        // no action
    }

    @Override
    public void setBack(final Integer v) {
        // no action
    }
}
