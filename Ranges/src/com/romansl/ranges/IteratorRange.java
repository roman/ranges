package com.romansl.ranges;

import java.util.Iterator;

public class IteratorRange<T> extends Range<T> {
    private final Iterator<T> iterator;
    private T value;
    private boolean empty;

    public IteratorRange(final Iterator<T> iterator) {
        this.iterator = iterator;

        popFront();
    }

    @Override
    public T front() {
        return value;
    }

    @Override
    public void popFront() {
        final boolean hasNext = iterator.hasNext();

        if (hasNext) {
            value = iterator.next();
        }

        empty = !hasNext;
    }

    @Override
    public boolean empty() {
        return empty;
    }

    @Override
    public void setFront(final T v) {
        throw new UnsupportedOperationException();
    }
}
