package com.romansl.ranges;

@SuppressWarnings("unchecked")
public class CastRange<T, S> extends Range<T> {
    private final Range<S> mSrc;

    public CastRange(final Range<S> src) {
        mSrc = src;
    }

    @Override
    public T front() {
        return (T) mSrc.front();
    }

    @Override
    public void popFront() {
        mSrc.popFront();
    }

    @Override
    public boolean empty() {
        return mSrc.empty();
    }

    @Override
    public void setFront(final T v) {
        mSrc.setFront((S) v);
    }
}
