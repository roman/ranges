package com.romansl.ranges;

public final class EmptyRange<T> extends BidirRange<T> {
    @Override
    public T front() {
        return null;
    }

    @Override
    public void popFront() {
        // no action
    }

    @Override
    public boolean empty() {
        return true;
    }

    @Override
    public void setFront(final T v) {
        // no action
    }

    @Override
    public T back() {
        return null;
    }

    @Override
    public void popBack() {
        // no action
    }

    @Override
    public void setBack(final T v) {
        // no action
    }

    @Override
    public void popBack(final int n) {
        // no action
    }

    @Override
    public void popFront(final int n) {
        // no action
    }
}
