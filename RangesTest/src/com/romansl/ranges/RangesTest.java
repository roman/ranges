package com.romansl.ranges;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.romansl.ranges.Ranges.*;

public class RangesTest extends TestCase {
    private final Integer[] ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private final Integer[] reversed = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    private final String[] strings = {"a", "b", "c", "d", "e"};
    private final String[] strings2 = {"a", null, "c", null, "e"};
    private static final Function<Integer, Integer> P2 = new Function<Integer, Integer>() {
        @Override
        public Integer apply(final Integer p) {
            return p + 2;
        }
    };
    private static final Function<Integer, Integer> M2 = new Function<Integer, Integer>() {
        @Override
        public Integer apply(final Integer p) {
            return p * 2;
        }
    };

    private static void assertRange(final String str, final Range<?> range) {
        assertEquals(str, Ranges.toString(range));
    }

    @Test
    public void testRangeIntInt() {
        final BidirRange<Integer> range = counter2(0, 9);
        for (final int i : ints) {
            assertEquals(i, (int) range.front());
            range.popFront();
        }
    }

    @Test
    public void testToString() {
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", counter(0, 9));
    }

    @Test
    public void testReverse() {
        assertRange("[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]", reverse(counter2(0, 9)));
    }

    @Test
    public void testRangeTArray() {
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", range(ints));
    }

    @Test
    public void testFind() {
        {
            final Range<Integer> found = find(range(ints), 4);
            assertFalse(found.empty());
            assertEquals(4, (int) found.front());
        }

        {
            final Range<Integer> found = find(range(ints), 555);
            assertTrue(found.empty());
        }

        {
            final Range<Integer> found = find(range(ints), (Integer) null);
            assertTrue(found.empty());
        }

        {
            final Range<String> found = find(range(strings2), (String) null);
            assertFalse(found.empty());
            assertNull(found.front());
        }

        {
            final Range<Integer> found = find(range(reversed), Predicates.equalsTo(5));
            assertFalse(found.empty());
            assertEquals(5, (int) found.front());
        }
    }

    @Test
    public void testIndexOf() {
        assertEquals(4, indexOf(range(ints), 4));
        assertEquals(-1, indexOf(range(ints), 44));
        assertEquals(2, indexOf(range(strings2), "c"));
        assertEquals(1, indexOf(range(strings2), (String) null));
    }

    @Test
    public void testCountOf() {
        assertEquals(1, countOf(range(ints), 4));
        assertEquals(0, countOf(range(ints), 44));
        assertEquals(3, countOf(range(strings2), Predicates.IS_NOT_NULL));
        assertEquals(2, countOf(range(strings2), (String) null));
    }

    @Test
    public void testZip() {
        assertRange("[Tuple(0, a), Tuple(1, b), Tuple(2, c), Tuple(3, d), Tuple(4, e)]", zip(range(ints), range(strings)));
    }

    @Test
    public void testFilter() {
        assertRange("[0, 2, 4, 6, 8]", filter(range(ints), Predicates.IS_EVEN));
        assertRange("[0, 2, 4, 6, 8]", filter(range2(ints), Predicates.IS_EVEN));
        assertRange("[8, 6, 4, 2, 0]", reverse(filter(range2(ints), Predicates.IS_EVEN)));
        assertRange("[]", filter(range(ints), Predicates.FALSE));
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", filter(range(ints), Predicates.TRUE));
        assertRange("[]", filter(range2(ints), Predicates.FALSE));
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", filter(range2(ints), Predicates.TRUE));
        assertRange("[]", reverse(filter(range2(ints), Predicates.FALSE)));
        assertRange("[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]", reverse(filter(range2(ints), Predicates.TRUE)));
    }

    @Test
    public void testTake() {
        assertRange("[0, 1, 2]", take(3, range(ints)));
        assertRange("[9, 8, 7]", reverse(take(3, range2(ints))));
    }

    @Test
    public void testChain() {
        assertRange("[1, 2, 3, 6, 7, 8, 9]", chain(counter(1, 3), counter(6, 9)));
        assertRange("[1, 2, 3]", chain(counter(1, 3), Ranges.<Integer>emptyRange()));
        assertRange("[6, 7, 8, 9]", chain(Ranges.<Integer>emptyRange(), counter(6, 9)));
        assertRange("[]", chain(Ranges.<Integer>emptyRange(), Ranges.<Integer>emptyRange()));

        assertRange("[]", chain());
        assertRange("[]", chain(Ranges.<Integer>emptyRange()));
        assertRange("[1, 2, 3]", chain(counter(1, 3)));
        assertRange("[1, 2, 3, 66, 67, 68, 69, 4, 5]", chain(counter(1, 3), counter(66, 69), counter(4, 5)));
        assertRange("[1, 2, 3, 66, 67, 68, 69]", chain(counter(1, 3), counter(66, 69), Ranges.<Integer>emptyRange()));
        assertRange("[66, 67, 68, 69, 4, 5]", chain(Ranges.<Integer>emptyRange(), counter(66, 69), counter(4, 5)));
        assertRange("[1, 2, 3, 4, 5]", chain(counter(1, 3), Ranges.<Integer>emptyRange(), counter(4, 5)));
        assertRange("[]", chain(Ranges.<Integer>emptyRange(), Ranges.<Integer>emptyRange(), Ranges.<Integer>emptyRange()));
    }

    @Test
    public void testCopy() {
        final Integer[] ints2 = ints.clone();
        final Range<Integer> range = copy(range(ints2), counter(10, 15));
        assertRange("[10, 11, 12, 13, 14, 15, 6, 7, 8, 9]", range(ints2));
        assertEquals((Integer) 6, range.front());

        copy(range, counter(10, 15));
        assertRange("[10, 11, 12, 13, 14, 15, 10, 11, 12, 13]", range(ints2));
        assertTrue(range.empty());
    }

    @Test
    public void testCopyToArray() {
        final Integer[] ints3 = ints.clone();
        final ArrayRange<Integer> copyResult = copy(ints3, counter(10, 15));
        assertRange("[10, 11, 12, 13, 14, 15, 6, 7, 8, 9]", range(ints3));
        assertEquals(Integer.valueOf(6), copyResult.front());
    }

    @Test
    public void testCopyFromArray() {
        final Integer[] array = new Integer[] {111, 112, 113};
        final Integer[] ints3 = ints.clone();
        final ArrayRange<Integer> copyResult = copy(range(ints3), array);
        assertRange("[111, 112, 113, 3, 4, 5, 6, 7, 8, 9]", range(ints3));
        assertEquals(Integer.valueOf(3), copyResult.front());
    }

    @Test
    public void testBiCopy() {
        // wrong!
        final Integer[] ints2 = ints.clone();
        copy(range2(ints2), reverse(range2(ints2)));
        assertRange("[9, 8, 7, 6, 5, 5, 6, 7, 8, 9]", range(ints2));

        // right!
        final Integer[] ints3 = ints.clone();
        bicopy(range2(ints3), reverse(range2(ints3)));
        assertRange("[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]", range(ints3));
    }

    @Test
    public void testAll1() {
        final Integer[] ints2 = ints.clone();
        copy(filter(range(ints2), Predicates.IS_EVEN), repeat(0));
        assertRange("[0, 1, 0, 3, 0, 5, 0, 7, 0, 9]", range(ints2));
    }

    @Test
    public void testReduce() {
        final int result = reduce(range(ints), new BinaryFunction<Integer, Integer, Integer>() {
            @Override
            public Integer apply(final Integer p1, final Integer p2) {
                return p1 + p2;
            }
        }, 0);

        assertEquals(45, result);

        final String result1 = reduce(range(ints), new BinaryFunction<String, Integer, String>() {
            @Override
            public String apply(final String p1, final Integer p2) {
                return p1 + "+" + p2;
            }
        }, "");

        assertEquals("+0+1+2+3+4+5+6+7+8+9", result1);
    }

    @Test
    public void testMap() {
        final Function<Integer, Integer> function = new Function<Integer, Integer>() {
            @Override
            public Integer apply(final Integer p) {
                return p * 2;
            }
        };

        final Range<Integer> result1 = map(range(ints), function);
        assertRange("[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]", result1);

        final BidirRange<Integer> result2 = map(range2(ints), function);
        assertRange("[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]", result2);

        final BidirRange<Integer> result3 = map(range2(ints), function);
        assertRange("[18, 16, 14, 12, 10, 8, 6, 4, 2, 0]", reverse(result3));
    }

    @Test
    public void testStepRange() {
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", step(range(ints), 1));
        assertRange("[0, 2, 4, 6, 8]", step(range(ints), 2));
        assertRange("[0, 3, 6, 9]", step(range(ints), 3));
        assertRange("[0]", step(range(ints), 10));
        assertRange("[0, 4, 8]", step(step(range(ints), 2), 2));
    }

    @Test
    public void testBidirStepRange() {
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", step(range2(ints), 1));
        assertRange("[0, 2, 4, 6, 8]", step(range2(ints), 2));
        assertRange("[0, 3, 6, 9]", step(range2(ints), 3));
        assertRange("[0]", step(range2(ints), 10));
        assertRange("[0, 4, 8]", step(step(range2(ints), 2), 2));

        assertRange("[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]", reverse(step(range2(ints), 1)));
        assertRange("[9, 7, 5, 3, 1]", reverse(step(range2(ints), 2)));
        assertRange("[9, 6, 3, 0]", reverse(step(range2(ints), 3)));
        assertRange("[9]", reverse(step(range2(ints), 10)));
        assertRange("[9, 5, 1]", reverse(step(step(range2(ints), 2), 2)));
    }

    @Test
    public void testTo() {
        final List<Integer> list = range(ints).toList();
        assertEquals("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", list.toString());

        final Integer[] array = range(ints).toArray(Integer.class);
        assertEquals("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", Arrays.toString(array));
    }

    @Test
    public void testMax() {
        final List<Integer> random = randomIntRange(100).take(10).toList();
        assertEquals(Collections.max(random), max(range(random)));
        assertNull(max(new EmptyRange<Comparable>()));
    }

    @Test
    public void testMin() {
        final List<Integer> random = randomIntRange(100).take(10).toList();
        assertEquals(Collections.min(random), min(range(random)));
        assertNull(min(new EmptyRange<Comparable>()));
    }

    @Test
    public void testCharacterRange() {
        assertRange("[H, e, l, l, o,  , W, o, r, l, d]", range("Hello World"));
        assertRange("[d, l, r, o, W,  , o, l, l, e, H]", reverse(range2("Hello World")));
    }

    @Test
    public void testSplit() {
        assertRange("[abc, def, ghi]", split("abc:def:ghi", ":"));
        assertRange("[abc, def, ]", split("abc:def:", ":"));
        assertRange("[abc, , ]", split("abc::", ":"));
        assertRange("[, , ]", split("::", ":"));
        assertRange("[abc]", split("abc", ":"));
        assertRange("[]", split("", ":"));
        assertRange("[]", split("", ":").map(Functions.TO_INT));
        assertRange("[, ]", split(":", ":"));
    }

    private Range<Range<Integer>> createRangeOfRanges(final Range<Integer>... ranges) {
        return range(ranges);
    }

    @Test
    public void testFlatten() {
        assertRange("[]", chain(createRangeOfRanges()));
        assertRange("[]", chain(createRangeOfRanges(range(new int[0]), range(new int[0]), range(new int[0]))));
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]",
                chain(createRangeOfRanges(range(ints), range(new int[0]), range(reversed))));
    }

    @Test
    public void testIteratorRange() {
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", range((Iterable) range(ints).toList()));
        assertRange("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]", range(range(ints).toList().iterator()));
        assertRange("[]", range((Iterable) new ArrayList<Object>(0)));
    }
}
