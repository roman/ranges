package com.romansl.ranges.demo;

import com.romansl.ranges.*;

import static com.romansl.ranges.Ranges.*;

public class Main {
    static Function<String, Integer> HEX_TO_INT = new Function<String, Integer>() {
        @Override
        public Integer apply(final String p) {
            return Integer.parseInt(p, 16);
        }
    };
    static BinaryFunction<Integer, Integer, Integer> SUMM = new BinaryFunction<Integer, Integer, Integer>() {
        @Override
        public Integer apply(final Integer p1, final Integer p2) {
            return p1 + p2;
        }
    };

    private static <T> void printRange(final String label, final Range<T> range) {
        System.out.print(label + ": ");
        System.out.println(Ranges.toString(range));
    }

    public static void main(final String[] args) {
        final Integer[] ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        final String[] strings = {"a", "b", "c", "d", "e"};

        // Prints 0 1 2 3 4 5 6 7 8 9
        System.out.print("range iteration");
        for(final Range<Integer> r = range(ints); !r.empty(); r.popFront()) {
            System.out.print(" " + r.front());
        }
        System.out.println();

        // Prints 0 1 2 3 4 5 6 7 8 9
        System.out.print("range iteration using iterators");
        for(final Integer i : range(ints)) {
            System.out.print(" " + i);
        }
        System.out.println();

        printRange("reverse int range", reverse(counter2(0, 3))); // [3, 2, 1, 0]
        printRange("reverse array", reverse(range2(ints))); // [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        printRange("zip", zip(infinite(0), range(strings))); // [Tuple(0, a), Tuple(1, b), Tuple(2, c), Tuple(3, d), Tuple(4, e)]
        printRange("filter not even", filter(range(ints), Predicates.not(Predicates.IS_EVEN))); // [1, 3, 5, 7, 9]
        printRange("chain2", chain(counter(1, 3), counter(6, 9))); // [1, 2, 3, 6, 7, 8, 9]
        printRange("chain3", chain(counter(1, 3), counter(66, 69), counter(4, 5))); // [1, 2, 3, 66, 67, 68, 69, 4, 5]
        printRange("take", take(3, range(strings))); // [a, b, c]
        printRange("sort", sort(take(10, randomIntRange(100)))); // [6, 18, 21, 22, 48, 55, 73, 84, 96, 96]

        {
            final Range<Integer> result = find(range(ints), 4);
            System.out.println("find result.empty: " + result.empty()); // false
            System.out.println("find result.value: " + result.front()); // 4

            System.out.println("indexOf: " + indexOf(range(ints), 4)); // 4
        }

        {
            final Integer[] ints2 = ints.clone();
            copy(filter(range(ints2), Predicates.IS_EVEN), repeat(0));
            printRange("replace all even by 0", range(ints2)); // [0, 1, 0, 3, 0, 5, 0, 7, 0, 9]
        }

        {
            final Integer[] ints1 = ints.clone();
            final Integer[] ints2 = new Integer[ints.length];
            copy(ints2, infinite(10));
            swap(range(ints1), range(ints2));
            printRange("swap", zip(range(ints1), range(ints2))); // [Tuple(10, 0), ..., Tuple(19, 9)]
        }

        {
            final String[] s1 = {"a", "b", "c"};
            final String[] s2 = {"1", "2", "3"};
            printRange("interchange", interchange(range(s1), range(s2))); // [a, 1, b, 2, c, 3]

            System.out.print("copy to StringBuilder: ");
            System.out.println(append(new StringBuilder(), interchange(range(s1), range(s2))).toString()); // a1b2c3
        }

        {
            System.out.print("join: ");
            System.out.println(join(range(ints), "-").toString()); // 0-1-2-3-4-5-6-7-8-9
        }

        {
            final String[] vals = {"a", null, "b", null, "c"};
            printRange("Predicates not null", filter(range(vals), Predicates.IS_NOT_NULL)); // [a, b, c]
            printRange("Predicates is null", filter(range(vals), Predicates.IS_NULL)); // [null, null]
        }

        {
            final Integer result = reduce(range(ints), SUMM, 0);
            System.out.println("reduce: " + result); // 45
        }

        {
            final Integer result = range(strings).map(HEX_TO_INT).reduce(SUMM, 0);
            System.out.println("map-reduce: " + result); // 60
        }
    }
}
